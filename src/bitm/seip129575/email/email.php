<?php
namespace App\Bitm\SEIP129575\Email;
use App\Bitm\SEIP129575\Message\Message;
use App\Bitm\SEIP129575\Utility\Utility;

Class Email
{
    public $id = "";
    public $email = "";
    public $conn;
    public $deleted_at;

    public function prepare($data = "")
    {
        if (array_key_exists("email", $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomicprojectb20") or die("Database conncection established failed");
    }

    public function store()
    {
        $query="INSERT INTO `atomicprojectb20`.`emailsubscription` (`email`) VALUES ('".$this->email."')";
        //echo $query;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            Utility::redirect('index.php');

        }
        else {
            Message::message("<div class=\"alert alert-info\">
  <strong>Error!</strong> Data has been stored successfully.
</div>");
            Utility::redirect('index.php');

        }

    }
    public  function index(){
        $_allEmail=array();
        $query= "SELECT * FROM `emailsubscription` where `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allEmail[]=$row;
        }
        return $_allEmail;
    }

    public function view(){
        $query="SELECT * FROM `emailsubscription` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }
    public function update(){
        $query="UPDATE `atomicprojectb20`.`emailsubscription` SET `email` = '".$this->email."' WHERE `emailsubscription`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }

    }
    public function delete()
    {
        $query = "DELETE FROM `atomicprojectb20`.`emailsubscription` WHERE `emailsubscription`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
        <div class=\"alert alert-info\">
        <strong>Deleted!</strong> Data has been deleted successfully.
        </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
                <div class=\"alert alert-info\">
                  <strong>Deleted!</strong> Data has not been deleted successfully.
                </div>");
            Utility::redirect("index.php");
        }

    }
    public function trash()
    {
        $this->deleted_at = time();

        $query = "UPDATE `atomicprojectb20`.`emailsubscription` SET `deleted_at` =".$this->deleted_at." WHERE `emailsubscription`.`id` = ".$this->id;
        echo $query;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
              <strong>Deleted!</strong> Data has been trashed successfully.
            </div>");
                        Utility::redirect("index.php");
                    } else {
                        Message::message("
            <div class=\"alert alert-info\">
              <strong>Deleted!</strong> Data has not been trashed successfully.
            </div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed()
    {
        $_allBook = array();
        $query = "SELECT * FROM `emailsubscription` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;


    }

    public function recover()
    {

        $query = "UPDATE `atomicprojectb20`.`emailsubscription` SET `deleted_at` = NULL WHERE `emailsubscription`.`id` = " . $this->id;
        echo $query;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been recovered successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been recovered successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicprojectb20`.`emailsubscription` SET `deleted_at` = NULL WHERE `emailsubscription`.`id` IN(" . $ids . ")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been recovered successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been recovered successfully.
</div>");
                Utility::redirect("index.php");
            }

        }

    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicprojectb20`.`emailsubscription` WHERE `emailsubscription`.`id` IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::redirect("index.php");
            }

        }
    }




//    public function update(){
//        $query="UPDATE `atomicprojectb20`.`emailsubscription` SET `email` = '".$this->email."' WHERE `emailsubscription`.`id` = ".$this->id;
//
////        $query="UPDATE `atomicprojectb20`.`emailsubscription` SET `email` = '".$this->email."' WHERE `emailsubscription`.`id` = ".$this->id;
//        $result= mysqli_query($this->conn,$query);
//        if($result){
//            Message::message("<div class=\"alert alert-info\">
//  <strong>Updated!</strong> Data has been updated successfully.
//</div>");
//            Utility::redirect('index.php');
//
//        }
//        else {
//            Message::message("<div class=\"alert alert-danger\">
//  <strong>Error!</strong> Data has not been updated successfully.
//</div>");
//            Utility::redirect('index.php');
//
//        }
//
//    }

//    public function prepare($data="")
//    {
//        if (array_key_exists("title", $data)) {
//            $this->title = $data['title'];
//        }
//        if (array_key_exists("id", $data)) {
//            $this->id = $data['id'];
//        }
//        return $this;
//    }
//
//    public function __construct(){
//        $this->conn= mysqli_connect("localhost","root","","atomicprojectb20") or die("Database connection establish failed");
//    }
//
//    public function store(){
//        $query="INSERT INTO `atomicprojectb20`.`booktitle` (`booktitle`) VALUES ('".$this->title."')";
//        //echo $query;
//        $result= mysqli_query($this->conn,$query);
//        if($result){
//            echo "Data has been inserted sucessfully";
//        }
//        else {
//            echo "Some error";
//        }
//    }
}