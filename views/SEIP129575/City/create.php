<!DOCTYPE html>
<html lang="en">
<head>
    <title>City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css"/>
    <scritp src="../../../Resource/bootstrap/js/bootstrap.min.js"></scritp>
</head>
<body>

<div class="container">
    <h2>User City</h2>

    <form role="form" method="post" action="store.php">
        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name" placeholder="Enter name"/>
            <label>City</label>
            <select name="city" class="form-control" id="city">
                <option value="Chittagong">Chittagong</option>
                <option value="Dhaka">Dhaka</option>
                <option value="Comilla">Comilla</option>
                <option value="Rajshahi">Rajshahi</option>
            </select>
            <input type="submit" class="btn btn-primary" value="Submit" />

        </div>
    </form>
</div>
</body>
</html>

