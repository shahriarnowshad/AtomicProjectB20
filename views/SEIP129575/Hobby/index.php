<?php
session_start();
include_once ('../../../vendor/autoload.php');
//var_dump($_POST);


use App\Bitm\SEIP129575\Hobby\Hobby;
use App\Bitm\SEIP129575\Utility\Utility;
use App\Bitm\SEIP129575\Message\Message;
$hobby = new Hobby();
$allHobby=$hobby->index();



?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css"/>
    <scritp src="../../../Resource/bootstrap/js/bootstrap.min.js"></scritp>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

</head>
<body>

<div class="container">
    <h2>All Hobby</h2>
    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>
    <a href="create.php" class="btn btn-info" role="button">Create Hobby</a> <a href="trashed.php" class="btn btn-primary" role="button">View Trashed Item</a><br><br>

    <div id="message">
        <?php
        if((array_key_exists('message',$_SESSION))&& !empty($_SESSION['message'])) {
            echo Message::message();
        }
        ?>
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>SL#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Hobbies</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach ($allHobby as $hobby){
                $sl++?>
                <tr>
                    <td><?php echo $sl?></td>
                    <td><?php echo $hobby->id ?></td>
                    <td><?php echo $hobby->name ?></td>
                    <td><?php echo $hobby->hobbies ?></td>
                    <td><a href="view.php?id=<?php echo $hobby->id ?>" class="btn btn-info" role="button">View</a>
                        <a href="edit.php?id=<?php echo $hobby->id ?>" class="btn btn-primary" role="button">Edit</a>
                        <a href="delete.php?id=<?php echo $hobby->id ?>" class="btn btn-danger delete" role="button"  >Delete</a>
                        <a href="trash.php?id=<?php echo $hobby->id ?>" class="btn btn-info" role="button">Trash</a>
                    </td>
                </tr>
            <?php }?>

            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $('#message').show().delay(2000).fadeOut();
    $(document).ready(function(){
        $(".delete").click(function(){
            if (!confirm("Do you want to delete")){
                return false;
            }
        });
    });
</script>

</body>
</html>

