<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP129575\Hobby\Hobby;
use App\Bitm\SEIP129575\Utility\Utility;

$hobby= new Hobby();
$singleItem=$hobby->prepare($_GET)->view();
//Utility::dd($singleItem);
$hobbylist=explode(",",$singleItem->hobbies);
//Utility::d($hobbylist);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css"/>
    <scritp src="../../../Resource/bootstrap/js/bootstrap.min.js"></scritp>
</head>
<body>

<div class="container">
    <h2>Select your hobby</h2>

    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label>Id:</label>
            <input type="text" name="id" value="<?php echo $singleItem->id; ?>" class="form-control">
            <label>Name:</label>
            <input type="text" name="name" value="<?php echo $singleItem->name; ?>" class="form-control">
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Playing Cricket" <?php if (in_array("Playing Cricket",$hobbylist)) echo "checked"; ?> />Playing Cricket</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Coding" <?php if (in_array("Coding",$hobbylist)) echo "checked"; ?>>Coding</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Browsing" <?php if (in_array("Browsing",$hobbylist)) echo "checked"; ?>>Browsing</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Book reading" <?php if (in_array("Book reading",$hobbylist)) echo "checked"; ?>>Book reading</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Gardening" <?php if (in_array("Gardening",$hobbylist)) echo "checked"; ?>>Gardening</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Teaching" <?php if (in_array("Teaching",$hobbylist)) echo "checked"; ?>>Teaching</label>
        </div>

    </form>
</div>

</body>
</html>

